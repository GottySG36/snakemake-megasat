
# Megasat snakemake analysis pipeline

# How to use this pipeline
In order to use thi pipeline, there are two documents to manually produce and one virtual environment to create. 

## Creating the virtual environment

If you already know how to create one, you are free to go ahead and use whichever framework you desire. The required dependencies are listed below. 

### Prerequisites

* python >= 3.9

Most linux systems should now come with a sufficiently recent python version. Note that this pipeline was created using python3.9. Most likely that any version above 3.5, would be fine, but that is untested. 

### Creating the environment itself

In this example, the environment will be created using python's module `venv`.

`python3 -m venv env-pipeline`

This command will create a virtual environment named `env-pipeline` in the current directory.

### Installing the required tools

Before we start, we need to activate the environment. 

`source env-pipeline/bin/activate` 

Finaly, the environment needs to have the following dependencies installed. All the rest is handled by the pipline. 

* snakemake
* pandas
* numpy
* pyyaml

`pip install -r requirements.txt`

### Other dependencies

* singularity

Singularity allows for the use of docker containers. This removes the to install any packages locally. Compute Canada already has it installed has a module. In the enventuality that it is not the case were you are running the anaysis, you will either need to install all the tools yourself or install singularity. The procedure for this is fairly strghtforward and can be found [here](#https://sylabs.io/guides/3.0/user-guide/installation.html).

### Files to prepare

#### config/samplesheet.csv

This file describes the samples to use as well as the names that will be given to them all along the analysis. It must contain four columns :

1. `path`
    * The path pointing to the raw data fastq file. Il can eiter be a relative or absolute path. 
2. `name`
    * The name that will be given to the file mentionned in path. To this name, we will add the paring information for paired-ends and the file extension. This name must be unique for each specific group of files. 
3. `pair`
    * For paired ends, the pair for that file, R1 or R2 depending on the file.
4. `lane`
    * If the sequencing was done on multiple lanes, we need to find that information here. For each lane, we need to repeat the sample names and the pairing information. The format is irrelevant here, but the information needs to be available anyway. Modt likely going to be something like `L00#` where `#` is the lane number. 

#### config/primers-info.tsv

This file describes the different primers that we expect to find in our data. It most contain 6 or 7 columns columns, the seventh being optional.

1. Name    
2. left_oligo
3. Rev_Comp_right_oligo
4. 5Flank
5. 3Flank
6. Repeat

Please refer yourselt to Megasat's documentation for more details ([documentation](https://github.com/beiko-lab/MEGASAT/blob/master/MEGASAT%20Documentation.pdf)).

## Starting the analysis

When every thing is ready, we can start the analysis. In order to make sure that every thing is in place, we can launch a dry run using snakemake. The following command will list allw the steps that snakemake would need to execute normally. This is especially useful to rapidely identify missing files or errors. 

`snakemake -npj1`

If this commdand returns no errors we can start the analysis.

`snakemake --use-singularity --singularity-args '<singularity runtime arguments, see below>' -kpj <nb threads>`

The first two options can ommitted if not using singularity. That should be the case if you installed the packages locally. Oterhwise, here is some more infomration on what each options do. 

* `--use-singularity`
    * Tells snakemake that he needs, when specified, to use containers to run certain tasks. 
* `--singularity-args '<ARGS...>`
    * Snakemake does not control singularity directly or automatically. This can sometimes be a problem. It is however possible to pass some options to singularity directly. In maybe 99% of the time, this will likely be used to specify specific directories to be used by the pipeline. **PLEASE NOTE** that singularity is meant to be a sandbox. It doesn't have access to the entire filesystem by default, only your home directory. In order to allw singularity access to different directories, we need to specify them using an additional argument. If we don't do that, we are going to get `No such file or directory` errors. 
    * This is the parameter to use `-B /PATH/WE/NEED/ACCESS/TO` and can be repeated as often as needed by the pipeline.
* `-k` 
    * Tells snakemake to carry on if any errors are encountered. This allows to complete as many tasks as possible instead of stopping immediately after all running tasks are done. 
* `-p`
    * This is a verbose option. Snakemake will print the commands used to the terminal. Useful if you want to know what is going on and what snakemake is doing. 
* `-j <INT>`
    * The number of threads that snakemake is allowed to used at any given moment. By default, snakemake uses only one. 

Here is another example of command that you could use :

`snakemake --use-singularity --singularity-args '-B /PATH/WE/NEED-1 -B /PATH/WE/NEED-2 [...]' -kpj <nb threads>`


