import pandas as pd

configfile: "config/config.yml"

include: "rules/trimming.rules"
include: "rules/qc.rules"
include: "rules/merge_lanes.rules"
include: "rules/merge_pairs.rules"
include: "rules/megasat.rules"

samplesheet_path = f"{config['path']}/{config['samplesheet']}"

samplesheet = pd.read_csv(samplesheet_path).sort_values(by=['name','pair','lane'])

rule all:
    input:
        [f"{config['path']}/analysis/merge/{s}_{p}.fastq.gz" 
            for s in samplesheet.name.unique()
            for p in samplesheet[samplesheet['name'] == s].pair], # merge output

        [f"{config['path']}/reports/qc/{s}_{p}_fastqc.{e}" 
            for s in samplesheet.name.unique()
            for p in samplesheet[samplesheet['name'] == s].pair
            for e in ["html", "zip"]], # qc output
        
        [f"{config['path']}/analysis/trimming/{s}_{p}.fastq.gz" 
            for s in samplesheet.name.unique()
            for p in samplesheet[samplesheet['name'] == s].pair], # trimming output
        
        [f"{config['path']}/analysis/merged_pairs/{s}.fastq" 
            for s in samplesheet.name.unique() ], # merging pairs output

        expand("{path}/results/Output_merged_pairs/{files}", 
            path = config['path'],
            files = ["Genotype.txt", "Number_Discarded.txt", 
                     "Number_Sorted.txt", "Number_Trimmed.txt"])  # Megasat output ==> Final output

